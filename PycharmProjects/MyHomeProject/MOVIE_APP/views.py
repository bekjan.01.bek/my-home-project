from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import DirectorListSerializer, DirectorDetailSerializer, MovieListSerializer, MovieDetailSerializer, \
    ReviewListSerializer, ReviewDetailSerializer, AverageSerializer
from .models import Director, Movie, Review

@api_view(['GET'])
def directors_list(request):
    directors = Director.objects.all()
    data = DirectorListSerializer(instance=directors, many=True).data
    return Response(data=data)


@api_view(['GET'])
def director_detail(request, director_id):
    def directors_detail(request, director_id):
        try:
            directors = DirectorListSerializer.objects.get(id=director_id)
        except DirectorListSerializer.DoesNotExist:
            return Response({'ERROR': f" director with id:{director_id} does not exists"},
                            status=404)

        serializer = DirectorDetailSerializer(instance=directors)
        return Response(serializer.data)


@api_view(['GET'])
def movie_list(request):
    movie = Movie.objects.all()
    data = MovieListSerializer(instance=movie, many=True).data
    return Response(data=data)


@api_view(['GET'])
def movie_detail(request, movie_id):
    try:
        movie = Movie.objects.get(id=movie_id)
    except Movie.DoesNotExist:
        return Response({'ERROR': f"Фильм с таким id {movie_id} не существует"})

    serializer = MovieDetailSerializer(instance=movie, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def review_list(request):
    review = Review.objects.all()
    data = ReviewListSerializer(instance=review, many=True).data
    return Response(data=data)


@api_view(['GET'])
def review_detail(request, review_id):
    try:
        review = Review.objects.get(id=review_id)
    except Review.DoesNotExist:
        return Response({'ERROR': f"Отзыва с таким id {review_id} не существует"})

    serializer = ReviewDetailSerializer(instance=review, many=False)
    return Response(serializer.data)

@api_view(['GET'])
def movie_reviews(request):
    reviews = Review.objects.all()
    serializer = AverageSerializer(instance=reviews, many=True)
    return Response(serializer.data)
