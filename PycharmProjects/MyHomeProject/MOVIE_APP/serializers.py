from rest_framework import serializers
from .models import Director, Review, Movie
from django.db.models import Avg

class DirectorListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Director
        fields = ('id', 'name', 'movies_count')

    def get_movies_count(self, obj):
        return obj.movies.count()



class DirectorDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Director
        fields = ('id', 'name')


class ReviewListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('id', 'text',)


class ReviewDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = "__all__"


class MovieListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('id', 'title', 'description', 'duration')


class MovieDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = "__all__"

class AverageSerializer(serializers.ModelSerializer):
    average_rating = serializers.SerializerMethodField()

    class Meta:
        model = Review
        fields = ('movie', 'text', 'average_rating')

    def get_average_rating(self, obj):
        average_rating = Review.objects.filter(movie=obj.movie).aggregate(avg_rating=Avg('rating'))
        return round(average_rating['avg_rating'], 1) if average_rating['avg_rating'] else 0.0
