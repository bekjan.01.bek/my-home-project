from django.contrib import admin
from django.urls import path
from MOVIE_APP import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/directors', views.directors_list),
    path('api/v1/directors/<int:director_id>/', views.director_detail),
    path('api/v1/movie', views.movie_list),
    path('api/v1/movie/<int:movie_id>/', views.movie_detail),
    path('api/v1/review', views.review_list),
    path('api/v1/review/<int:review_id>/', views.review_detail),
    path('api/v1/movies/reviews/', views.movie_reviews, name='movie_reviews'),
]


# ВЫВЕСТИ СПИСОК РЕЖИССЕРОВ /API/V1/DIRECTORS/
#
# ВЫВЕСТИ ОДНОГО РЕЖИССЕРА   /API/V1/DIRECTORS/<INT:ID>/
#
# ВЫВЕСТИ СПИСОК ФИЛЬМОВ      /API/V1/MOVIES/
#
# ВЫВЕСТИ ОДИН ФИЛЬМ             /API/V1/MOVIES/<INT:ID>/
#
# ВЫВЕСТИ СПИСОК ОТЗЫВОВ       /API/V1/REVIEWS/
#
# ВЫВЕСТИ ОДИН ОТЗЫВ              /API/V1/REVIEWS/<INT:ID>/
