import time
from aiogram import types
from config import bot
from collections import defaultdict

FORBIDDEN_WORDS = ("дебил", "блять", "чурка", "сгн", "коток", "похуй", "хуй")

warnings = defaultdict(int)

async def catch_bad_words(message: types.Message):
    if message.chat.type != "private":
        for word in FORBIDDEN_WORDS:
            if word in message.text.casefold():
                chat_id = message.chat.id
                user_id = message.from_user.id
                # Проверьте, является ли пользователь админом
                admins = await bot.get_chat_administrators(chat_id)
                admin_ids = [admin.user.id for admin in admins]
                if user_id in admin_ids:
                    await message.reply("Уважаемый админ, пожалуйста, подбирайте слова!")
                else:
                    await message.reply("Не выражайтесь\n "
                                        "               ©Капитан Америка")
                    warnings[user_id] += 1
                    if warnings[user_id] == 1:
                        until_date = int(time.time()) + 3600  # Бан на 1 час
                    elif warnings[user_id] == 2:
                        until_date = int(time.time()) + 86400  # Бан на 1 день
                    else:
                        until_date = 0  # Бан навсегда
                    await bot.kick_chat_member(chat_id, user_id, until_date=until_date)
                await message.delete()
                break


# async def example(message: types.Message):
# #     print(f"{message.chat.type=}")
# #     print(f"{message.reply_to_message=}")
# #
# #     if message.chat.type != 'private':
# #         await message.answer("Это сообщение было отправлерно не в ЛС")
# #     else:
# #         await message.answer("Этот бот работает только в группе")