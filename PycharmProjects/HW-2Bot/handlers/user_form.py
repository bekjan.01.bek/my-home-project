from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from pprint import pprint
from db.request import save_user_results

class Form(StatesGroup):
    name = State()
    surname = State()
    age = State()
    gender = State()
    city = State()
    address = State()


async def cmd_start(message: types.Message):
    await Form.name.set()
    await message.reply("Привет! Я буду задавать тебе несколько вопросов. Давай начнем. Как тебя зовут?")


async def your_name(message: types.Message, state: FSMContext):
    name = message.text
    if not name.isalpha():
        await message.answer('Пожалуйста введите букву')
    elif len(name) < 2 or len(name) > 20:
        await message.answer("Имя должно состоять из 2-х букв и более, и не более 20 букв")
    else:
        async with state.proxy() as data:
            data['name'] = name

        await Form.next()
        await message.reply("Спасибо! Теперь введи свою фамилию?")


async def your_surname(message: types.Message, state: FSMContext):
    surname = message.text
    if not surname.isalpha():
        await message.answer('Пожалуйста введите букву')
    elif len(surname) < 2 or len(surname) > 50:
        await message.answer("Фамилия должна состоять из 2-х букв и более, и не более 50 букв")
    else:
        async with state.proxy() as data:
            data['surname'] = surname

        await Form.next()
        await message.reply("Отлично! Сколько тебе лет?")


async def your_age(message: types.Message, state: FSMContext):
    age = message.text
    if not age.isdigit():
        await message.answer("Пожалуйста, введите возраст числом.")
    elif int(age) < 15 or int(age) > 99:
        await message.answer("Анкета создается для тех, кому от 15 до 99 лет")
    else:
        async with state.proxy() as data:
            data["age"] = int(age)
            pprint(data.as_dict())

            kb = types.ReplyKeyboardMarkup()
            kb.add("Мужской", "Женский")
            await Form.next()
            await message.reply("Укажи свой пол:", reply_markup=kb)

async def your_gender(message: types.Message, state: FSMContext):
        gender = message.text

        if gender not in ('Мужской', 'Женский'):
            await message.reply("Пожалуйста, нажмите на кнопку")
        else:

            async with state.proxy() as data:
                data['gender'] = message.text
                pprint(data.as_dict())

                save_user_results(data.as_dict())

        await Form.next()
        await message.reply("Какой у тебя город?")


async def your_city(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['city'] = message.text

    await Form.next()
    await message.reply("Какой у тебя адрес??")


async def your_address(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['address'] = message.text

    your_data = await state.get_data()
    data = await state.get_data()
    await message.reply(f"Спасибо за ответы!\n"
                        f"Имя: {data['name']}\n"
                        f"Фамилия: {your_data['surname']}\n"
                        f"Возраст: {data['age']}\n"
                        f"Пол: {data['gender']}\n"
                        f"Город: {your_data['city']}\n"
                        f"Адрес: {your_data['address']}")

    await state.finish()


def register_survey_handlers(dp: Dispatcher):
    dp.register_message_handler(cmd_start, commands=["start"])
    dp.register_message_handler(your_name, state=Form.name)
    dp.register_message_handler(your_surname, state=Form.surname)
    dp.register_message_handler(your_age, state=Form.age)
    dp.register_message_handler(your_gender, state=Form.gender)
    dp.register_message_handler(your_city, state=Form.city)
    dp.register_message_handler(your_address, state=Form.address)
