from django.db import models

class BookProgramLang(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='')
    description = models.TextField()
    cost = models.DecimalField(max_digits=10, decimal_places=1)
    color = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title